import time

import pytest
from selenium.webdriver import Chrome, ChromeOptions
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

manager = ChromeDriverManager()

options = ChromeOptions()

url = 'https://demo.app.stack-it.ru/fl/'
log = 'DEMOWEB'
paswrd = 'awdrgy'
district = 'район'


@pytest.fixture(scope='function')
def login():
    driver = Chrome()
    driver.get(url)
    time.sleep(5)
    driver.find_element(By.XPATH, '//input[contains(@data-cy, "login")]').send_keys(log)
    driver.find_element(By.XPATH, '//input[contains(@data-cy, "password")]').send_keys(paswrd)
    driver.find_element(By.XPATH, '//button[contains(@data-cy, "submit-btn")]').click()
    time.sleep(4)
    yield driver
    driver.quit()


 def test_district(login):
     login.find_element(By.XPATH, '//a[contains(@data-test-id, "Адресный фонд")]').click()
     time.sleep(1)
     login.find_element(By.XPATH, '//a[contains(@data-test-id, "Адреса проживающих")]').click()
     time.sleep(1)
     login.find_element(By.XPATH, '//button[contains(@data-cy, "btn-add")]').click()
     login.find_element(By.XPATH, '//div[contains(@data-cy, "stack-menu-list-item")]').click()
     time.sleep(1)
     login.find_element(By.XPATH, '//input[contains(@data-cy, "stack-input")]').send_keys(district)     time.sleep(4)#     login.find_element(By.XPATH, '//button[contains(@data-cy, "btn-save")]').click()
     assert 'район' in login.page_source
